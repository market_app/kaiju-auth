Auth services, tables and functions.

Compatibility
-------------

**Python**: 3.8

Summary
-------

**login.py** - various user login services
**models.py** - database structures
**services.py** - all services imports for convenience
**sessions.py** - session management services
**tokens.py** - jwt token management services
**users.py** - user data management
**views.py** - HTTP views for user related operations

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv local 3.7.5 3.8.1 3.9.0```

Then you can run `tox` command to test against all of them.

Documentation
-------------

sphinx
^^^^^^

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
