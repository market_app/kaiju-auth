"""
Login service is an endpoint for all user session operations. It can combine
multiple methods of authentication and session management.

There are three other services it may depend on:

- :class:`.UserService` - stores user/group data
- :class:`.HTTPSessionService` - stateful session management
- :class:`.JWTService` - this service can create, sign and validate JWT tokens

For example, we can define a fully functional service able to create tokens
and provide user methods and stored sessions as well.

.. code-block:: python

    my_login_service = HTTPLoginService(
        app=my_app,
        user_service=my_user_service,
        session_service=my_session_service,
        token_service=my_jwt_service,
        logger=my_app_logger
    )

Or you can create a simple token verification service which uses a shared cache
to verify tokens signatures but doesn't provide login/user methods. It may be
useful for microservices.

.. code-block:: python

    my_login_service = HTTPLoginService(
        app=my_app,
        user_service=False,
        session_service=False,
        token_service=my_jwt_service,
        logger=my_app_logger
    )

.. attention::

    Shared cache is essential in the latter case, because if a service can't
    access users/sessions table it should at least be able to read shared
    public RSA keys to verify token signatures. Otherwise all received tokens
    will be automatically marked as invalid.


Classes
-------

"""


from __future__ import annotations

import abc
import asyncio
import functools
import time
from typing import Optional, Union

try:
    from typing import TypedDict
except ImportError:
    # python 3.6 compatibility
    from typing_extensions import TypedDict

from aiohttp.web import Request

from kaiju_tools.exceptions import NotAuthorized, Conflict
from kaiju_tools.services import Service, ContextableService
from kaiju_tools.rpc.client import RPCClientService, AbstractTokenInterface
from kaiju_tools.rpc.abc import Session
from kaiju_tools.rpc import AbstractRPCCompatible

from .users import UserService
from .tokens import JWTService
from .sessions import HTTPSessionService

__all__ = (
    'HTTPLoginService', 'ErrorCodes', 'JWTClientService'
)


class ErrorCodes:
    AUTH_METHOD_DISABLED = 'auth.auth_method_disabled'


class HTTPLoginService(Service, AbstractRPCCompatible):
    """User service what is compatible with `HTTPCompatibleJSONQueueServer`
    rpc service.

    It is used for session/cookie based authentication and profile management.

    :param app: web app
    :param session_service: http session service instance or name
    :param user_service: user management service name or instance
    :param session_refresh_interval: user permissions stored in session's
    cache will be refreshed periodically, this parameter determines the
    refresh interval (in seconds), you shouldn't use too short intervals
    because it slows the request performance
    :param permissions: custom RPC permissions
    :param logger:
    """

    class _H_TokenResponse(TypedDict):
        access: str
        refresh: str
        exp: int
        refresh_exp: int

    service_name = 'auth'
    session_service_class = HTTPSessionService
    token_service_class = JWTService
    user_service_class = UserService
    ErrorCodes = ErrorCodes
    SESSION_REFRESH_INTERVAL = 300
    _session_refresh_key = 'updated'

    def __init__(
            self, app,
            user_service: Union[str, user_service_class] = None,
            session_service: Union[str, session_service_class] = None,
            token_service: Union[str, token_service_class] = None,
            session_refresh_interval: int = SESSION_REFRESH_INTERVAL,
            permissions: dict = None,
            logger=None):

        super().__init__(app=app, logger=logger)
        AbstractRPCCompatible.__init__(self, permissions=permissions)
        self._session_refresh_interval = session_refresh_interval
        self._user_service = self.discover_service(
            user_service, cls=self.user_service_class, required=False)
        self._session_service = self.discover_service(
            session_service, cls=self.session_service_class, required=False)
        self._token_service = self.discover_service(
            token_service, cls=self.token_service_class, required=False)

    @property
    def fake_routes(self):
        """Fake routes. They are used for faking RPC in requests that need an
        actual HTTP request object in input."""

        if self.user_backend_enabled and self.session_backend_enabled:
            routes = {
                f'{self.service_name}.register': self.register,
                f'{self.service_name}.login': self.login,
                f'{self.service_name}.logout': self.logout,
                f'{self.service_name}.change_password': self.change_password
            }
        else:
            routes = {}

        return routes

    @property
    def routes(self):

        routes = {}

        if self.user_backend_enabled:
            routes['update_profile'] = self.update_profile
            routes['user_info'] = self.get_user

        if self.token_backend_enabled:
            routes['jwt_get'] = self.jwt_get
            routes['jwt_refresh'] = self.jwt_refresh

        return routes

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_USER_PERMISSION,
            'jwt_get_by_user_id': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION,
            'jwt_get': self.PermissionKeys.GLOBAL_GUEST_PERMISSION,
            'jwt_refresh': self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    async def register(self, request: Request, username: str, email: str, password: str):
        """Registers a new user and performs a login."""

        self._check_user_service_is_available()
        self._check_session_service_is_available()
        await self._user_service.register(username, email, password)
        return await self.login(request, username, password)

    async def jwt_get_by_user_id(self, user_id, ttl=None) -> _H_TokenResponse:
        """
        This method creates a JWT token for a specific user. It can be used
        by the system to create some user level access tokens while not requiring
        the specific user to be actually logged in.

        .. attention::

            This method should not be exposed to non-system users.

        :param user_id: uuid
        :param ttl: token lifetime in seconds
        """

        self._check_user_service_is_available()
        self._check_token_service_is_available()
        user = await self._user_service.get_user_info_by_user_id(user_id)
        user['id'] = user_id
        return await self._create_tokens(user, ttl=ttl)

    async def jwt_get(self, username: str, password: str) -> _H_TokenResponse:
        """Logs user in and returns a JWT token pair."""

        self._check_user_service_is_available()
        self._check_token_service_is_available()
        user = await self._user_service.auth(username=username, password=password)
        return await self._create_tokens(user)

    async def _create_tokens(self, user_info: dict, ttl=None):
        data = {
            'user_id': user_info['id'],
            'permissions': user_info['permissions'],
        }
        access, refresh = await self._token_service.generate_token_pair(ttl=ttl, **data)
        return {
            'access': access.serialize(compact=True),
            'refresh': refresh.serialize(compact=True),
            'exp': access.claims_data['exp'],
            'refresh_exp': refresh.claims_data['exp']
        }

    async def jwt_refresh(self, refresh: str) -> _H_TokenResponse:
        """Refreshes an auth token using a refresh token."""

        self._check_token_service_is_available()
        access, refresh = await self._token_service.refresh_token(refresh)
        return {
            'access': access.serialize(compact=True),
            'refresh': refresh.serialize(compact=True),
            'exp': access.claims_data['exp'],
            'refresh_exp': refresh.claims_data['exp']
        }

    async def jwt_get_session(self, request: Request) -> Session:
        """
        Validates a JWT token and returns a user quasi-session if available.
        A guest session will be returned otherwise.
        """

        self._check_token_service_is_available()
        if self._token_service:
            token = self._extract_token(request)
            if token:
                token = await self._token_service.verify_token(token)
                session = token.claims_data
                return session
        return await self.get_guest_session()

    async def get_guest_session(self) -> Session:
        permissions = await self._user_service.get_default_nonlogin_permissions()
        return {
            'user_id': None,
            'permissions': permissions,
            #'stored': False,
            #'data': {}
        }

    async def login(self, request: Request, username: str, password: str):
        """Logs user in and creates a new session. If old user is currently present,
        it will log him out first."""

        self._check_user_service_is_available()
        self._check_session_service_is_available()
        session = await self.get_session(request)
        user = await self.get_user(session)
        if user:
            await self.logout(request)
        user = await self._user_service.auth(username=username, password=password)
        data = self._get_session_data(user)
        await self._session_service.delete_session(request)
        session = await self._session_service.create_session(
            request,
            user_id=user['id'],
            permissions=user['permissions'],
            data=data
        )
        session._changed = True
        return await self.get_user(session)

    async def logout(self, request: Request):
        """Logout an authorized user and clear the session."""

        self._check_session_service_is_available()
        session = await self.get_session(request)
        if await self.get_user(session):
            await self._session_service.delete_session(request)
        else:
            raise NotAuthorized('Authorization required.')

    async def change_password(self, request: Request, username: str, password: str, new_password: str):
        """Changes user password to a new one and performs a fresh login."""

        self._check_user_service_is_available()
        self._check_session_service_is_available()
        session = await self.get_session(request)
        if await self.get_user(session):
            await self._user_service.change_password(username, password, new_password)
            await self.logout(request)
            return await self.login(request, username, new_password)
        else:
            raise NotAuthorized('Authorization required.')

    async def update_profile(self, session: Session, settings: dict) -> dict:
        """Updates user profile metadata."""

        self._check_user_service_is_available()
        settings = await self._user_service.update_profile(session, settings)
        settings = settings['settings']
        session['settings'] = settings
        return settings

    async def get_user(self, session: Session):
        """Returns a complete information about the current logged user."""

        self._check_user_service_is_available()
        user_id = session.get('user_id')
        if user_id:
            return await self._user_service.get_user_info(session)

    async def get_session(self, request: Request):
        """Writes a user session and session permissions. Update permissions
        if required."""

        self._check_user_service_is_available()
        self._check_session_service_is_available()
        session = await self._session_service.get_session(request)
        user_id = session.get('user_id')

        if user_id:  # session has a real user
            last_updated = session.get('updated', 0)
            if last_updated + self._session_refresh_interval < time.time():
                data = await self._user_service._get_user_and_permissions(user_id)
                data = self._get_session_data(data)
                session._mapping.update(data)

        if session.get('permissions') is None:
            session['permissions'] = await self._user_service.get_default_nonlogin_permissions()

        return session

    @staticmethod
    def _get_session_data(user: dict) -> dict:
        return {
            'settings': user['settings'],
            'updated': int(time.time())
        }

    @property
    def session_backend_enabled(self) -> bool:
        return self._session_service is not None

    def _check_session_service_is_available(self):
        if not self._session_service:
            raise Conflict(
                'Session service is disabled.',
                service=self.service_name,
                code=self.ErrorCodes.AUTH_METHOD_DISABLED)

    @property
    def user_backend_enabled(self) -> bool:
        return self._user_service is not None

    def _check_user_service_is_available(self):
        if not self._user_service:
            raise Conflict(
                'User service is disabled.',
                service=self.service_name,
                code=self.ErrorCodes.AUTH_METHOD_DISABLED)

    @property
    def token_backend_enabled(self) -> bool:
        return self._token_service is not None

    def _check_token_service_is_available(self):
        if not self._token_service:
            raise Conflict(
                'Token service is disabled.',
                service=self.service_name,
                code=self.ErrorCodes.AUTH_METHOD_DISABLED)

    @staticmethod
    def has_token(request: Request):
        """Special method required by HTTP views."""

        return 'Authorization' in request.headers

    def _extract_token(self, request: Request) -> Optional[str]:
        """Extracts a JWT token from request headers if possible."""

        if self.has_token(request):
            auth_header = request.headers['Authorization'].strip()
            if auth_header.startswith('Bearer'):
                token = auth_header.replace('Bearer', '', 1).strip()
                if token:
                    return token


class JWTClientService(ContextableService, AbstractTokenInterface):
    """
    JWT token client. It will automatically acquire and refresh JWT tokens.

    Usage example:

    Create a service or a service config for the client.

    .. code-block:: python

        jwt_client_settings = {
          'cls': 'JWTClientService',
          'settings': {
            'transport': '<your_http_client_service_name>',
            'auth': {
              'username': 'bob',
              'password': 'qwerty'
            }
          }
        }

    After the initialization process you will be able to access JWT tokens.

    .. code-block:: python

        token = await jwt_client.get_token()
        my_client.call(headers={'Authorization': f'Bearer {token}'})


    Token lifetimes will be automatically monitored by the service.

    :param app:
    :param auth: authentication data (username and password, see `_H_AuthParams`)
    :param login_service: login service, should be either an RPC client or
        a local login service
    :param logger:
    """

    class _H_AuthParams(TypedDict):
        username: str
        password: str

    EXP_WINDOW = 5

    def __init__(
            self, app,
            auth: _H_AuthParams,
            login_service: Union[str, HTTPLoginService, RPCClientService],
            logger=None):

        super().__init__(app=app, logger=logger)
        self._login_service = login_service
        self._login_method = None
        self._refresh_method = None
        self._auth = auth
        self._access, self._refresh, self._exp, self._refresh_exp = None, None, None, None
        self._loop, self._task, self._closing = None, None, None

    async def init(self):
        await super().init()

        self._login_service = self.discover_service(self._login_service)

        if isinstance(self._login_service, HTTPLoginService):
            self._login_method = self._jwt_get_local
            self._refresh_method = self._jwt_refresh_local
        else:
            self._login_method = self._jwt_get
            self._refresh_method = self._jwt_refresh

        self.logger.debug('Receiving a fresh JWT token.')
        await self._login_task()
        self._closing = False
        self.logger.debug('Starting a token refresh loop.')
        self._loop = asyncio.create_task(self._refresh_loop())

    async def close(self):
        self._closing = True
        self.logger.debug('Terminating the token refresh loop.')
        self._loop.cancel()
        if self._task and not self._task.done():
            await self._task
        await super().close()
        self._closing = None

    @property
    def closed(self) -> bool:
        return self._closing is None or self._closing is False

    async def get_token(self) -> str:
        """This method should be used to get a fresh access token."""

        if self._task and not self._task.done():
            await self._task
        return self._access

    async def _jwt_get(self) -> HTTPLoginService._H_TokenResponse:
        """Client method for `jwt_get` of a login service."""

        result = await self._login_service.call(
            method=f'{HTTPLoginService.service_name}.jwt_get',
            params=self._auth, raise_exception=True,
            headers={'Authorization': ''})
        return result

    async def _jwt_refresh(self) -> HTTPLoginService._H_TokenResponse:
        """Client method for `jwt_refresh` of a login service."""

        result = await self._login_service.call(
            method=f'{HTTPLoginService.service_name}.jwt_refresh',
            params={'refresh': self._refresh}, raise_exception=True,
            headers={'Authorization': f'Bearer {self._access}'})
        return result

    async def _jwt_get_local(self) -> HTTPLoginService._H_TokenResponse:
        result = await self._login_service.jwt_get(**self._auth)
        return result

    async def _jwt_refresh_local(self) -> HTTPLoginService._H_TokenResponse:
        result = await self._login_service.jwt_refresh(refresh=self._refresh)
        return result

    async def _refresh_loop(self):
        """Runs automatically for JWT auto-refresh."""

        while not self._closing:
            dt = self._get_timeout(self._exp)
            if dt >= 0:
                self.logger.debug('Waiting %d until refresh.', int(dt))
                await asyncio.sleep(dt)
            if self._get_timeout(self._refresh_exp) > 0:
                self._task = asyncio.create_task(self._refresh_task())
            else:
                self._task = asyncio.create_task(self._login_task())
            self.logger.debug('Awaiting the refresh task.')
            await self._task
            self._task = None

    async def _login_task(self):
        try:
            data = await self._login_method()
        except Exception as exc:
            self.logger.error('Cannot login: %s', exc)
        else:
            self._set_values(data)

    async def _refresh_task(self):
        try:
            data = await self._refresh_method()
        except Exception as exc:
            self.logger.error('Cannot renew the token: %s', exc)
        else:
            self._set_values(data)

    def _set_values(self, data: dict):
        """Stores new token data."""

        self._access = data['access']
        self._refresh = data['refresh']
        self._exp = data['exp']
        self._refresh_exp = data['refresh_exp']

    def _get_timeout(self, value: float) -> float:
        """Returns time until next token expiration."""

        if value:
            dt = max(0., value - time.time() - self.EXP_WINDOW)
            return dt
        else:
            return 0.
