import asyncio

from kaiju_tools.http.client import HTTPService
from kaiju_tools.rpc.services import JSONRPCServer, RPCClientService

from ..services import *
from ..views import *


async def test_views(
        web_application, aiohttp_client, database, database_service, permission,
        group, user, logger):
    route = RestrictedJSONRPCView.route
    web_application.router.add_view(route, RestrictedJSONRPCView)

    async def _test_token_auth(client, auth_token):
        resp = await client.post(
            route,
            json={'method': 'auth.user_info'},
            headers={'Authorization': f' Bearer {auth_token}'}
        )
        assert resp.status == 200
        data = await resp.json()
        logger.debug(data)
        assert data['result']['settings']['language'] == 'ru'

    group_id, permission_id = group['id'], permission['id']
    permission_service = PermissionService(
        app=web_application, database_service=database_service, logger=logger)
    group_service = GroupService(
        app=web_application, database_service=database_service,
        permission_service=permission_service, logger=logger)
    user_service = UserService(
        app=web_application, database_service=database_service, default_group=group_id,
        default_nonlogin_group=None, group_service=group_service, logger=logger)
    session_service = SessionService(
        app=web_application, database_service=database_service, logger=logger)
    http_session_service = HTTPSessionService(
        app=web_application, session_service=session_service, logger=logger)
    keystore_service = KeystoreService(app=None, cache_service=False, logger=logger)
    token_service = JWTService(app=None, keystore=keystore_service, logger=logger)
    login_service = HTTPLoginService(
        app=web_application, user_service=user_service, session_service=http_session_service,
        token_service=token_service, logger=logger)
    rpc = JSONRPCServer(app=web_application)
    rpc.register_service(login_service.service_name, login_service)

    web_application.services = {
        http_session_service.service_name: http_session_service,
        login_service.service_name: login_service,
        rpc.service_name: rpc
    }

    client = await aiohttp_client(web_application, server_kwargs={'port': 7777})

    async with database_service:
        async with rpc:
            await permission_service.create(permission)
            await group_service.create(group)
            await group_service.modify_permissions(group_id, {permission_id: True})

            logger.info('-- TESTING /register --')
            resp = await client.post(route, json={'method': 'auth.register', 'params': user})
            logger.debug(resp.cookies)
            assert resp.status == 200
            data = await resp.text()
            logger.debug(data)

            logger.info('-- TESTING /logout --')
            resp = await client.post(route, json={'method': 'auth.logout'}, cookies=resp.cookies)
            logger.debug(resp.cookies)
            assert resp.status == 200
            data = await resp.json()
            assert data['result'] is None

            logger.info('-- TESTING /login --')
            resp = await client.post(
                route,
                json={
                    'method': 'auth.login',
                    'params': {
                        'username': user['username'],
                        'password': user['password']
                    }
                },
                cookies=resp.cookies
            )
            logger.debug(resp.cookies)
            assert resp.status == 200
            data = await resp.json()
            logger.debug(data)
            assert 'permissions' in data['result']

            logger.info('-- TESTING /profile --')
            resp = await client.post(
                route,
                json={
                    'method': 'auth.update_profile',
                    'params': {
                        'settings': {
                            'language': 'ru'
                        }
                    }
                },
                cookies=resp.cookies
            )
            logger.debug(resp.cookies)
            assert resp.status == 200
            data = await resp.json()
            logger.debug(data)
            assert data['result']['language'] == 'ru'

            logger.info('-- TESTING /get_user --')
            resp = await client.post(route, json={'method': 'auth.user_info'}, cookies=resp.cookies)
            logger.debug(resp.cookies)
            assert resp.status == 200
            data = await resp.json()
            logger.debug(data)
            assert data['result']['settings']['language'] == 'ru'

            await client.post(route, json={'method': 'auth.logout'}, cookies=resp.cookies)

            logger.info('-- TESTING token auth --')
            resp = await client.post(
                route,
                json={
                    'method': 'auth.jwt_get',
                    'params': {
                        'username': user['username'],
                        'password': user['password']
                    }
                }
            )
            assert resp.status == 200
            data = await resp.json()
            logger.debug(data)
            auth_token = data['result']['access']

            await _test_token_auth(client, auth_token)

            logger.info('Testing HTTP token client')

            http_client = HTTPService(
                host='http://localhost:7777',
                app=None, logger=logger
            )
            rpc_client = RPCClientService(
                app=None, logger=logger, uri=route,
                transport=http_client
            )
            token_client = JWTClientService(
                login_service=rpc_client,
                app=None,
                auth={
                    'username': user['username'],
                    'password': user['password']
                },
                logger=logger
            )

            token_service._access_token_ttl = 60

            async with http_client:
                async with rpc_client:
                    async with token_client:
                        auth_token = await token_client.get_token()
                        await _test_token_auth(client, auth_token)
                        await asyncio.sleep(1)
