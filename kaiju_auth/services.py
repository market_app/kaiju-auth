"""
This module contains imports of all the auth services. You can use it to simply
register all the auth classes in your application's class registry.

Services object relation
------------------------

This diagram shows how user services are related. An instance of a service contains
links to one or more other services.

.. image:: ../images/user_services_object_relation.png

"""

from .tokens import KeystoreService, JWTService
from .sessions import SessionService, HTTPSessionService
from .login import HTTPLoginService, JWTClientService
from .users import PermissionService, GroupService, UserService
from .fixtures import UserFixtureService
from .permissions_gui import UserGUIService, GroupGUIService
